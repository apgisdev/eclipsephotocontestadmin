import { Component } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Headers, RequestOptions, Http, Response } from '@angular/http';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  private title;
  private username;
  private password;
  // private http: Http;
  // pathToAPConnectLogo = "APConnectLogo.png";
  // constructor(private http: Http){
  constructor(private http: Http){
    console.log('constructor');
    this.title = 'APConnect web Admin';
  }
  onkeyupUsername(args){
    this.username = args.target.value;
  }
  onkeyupPassword(args){
    this.password = args.target.value;
  }
  attemptLogin(){

    console.log('button pressed');
    let mail = {
      username: this.username,
      password: this.password
    }
    this.requestData('POST', '/users/login', '', '', JSON.stringify(mail))
    .subscribe(
      data => {
        console.log('data : ' + data);
      },
      error => {
        console.log('error : ' + error);
      },
      () =>{
        console.log('done');
      }
    )


  }
  requestData(_method:string, _endpoint:string, _url?:string, _responsetype?:string, _params?:string): Observable<any> {
    var defaultServerTarget, responseType, tagDataService;

    defaultServerTarget = "http://apnsgis3.apsu.edu:8080";

    console.log('_url : ' + _url);
    //Check if _params was passed and if not, make it empty.
    let params;
    if(_params){ params = _params;} else {params = ""}

    //Check if _url was passed and if not, make it the sandboxUrl
    let urlToSend;
    urlToSend = _url + _endpoint;
    if(_url || _url !== ''){urlToSend = _url + _endpoint} else {urlToSend = defaultServerTarget + _endpoint;}
    console.log('urlToSend : ' + urlToSend);

    let myheaders = new Headers();
    myheaders.set('x-authorization', 'B9CFAE624E2F1F5981738140B1E0259A2EFD15B4073148AD61664073FFBE4357849EEE4C97126D2887539C00C8768516543999F5E0777428');
    myheaders.set('Access-Control-Allow-Origin', '*');

    if(_responsetype === "text"){
      console.log('endpoint: '+_endpoint);
      myheaders.append('Content-Type', 'text/plain');
      responseType = "text"
    } else {
      console.log('endpoint: '+_endpoint+'Content-Type application/json');
      myheaders.append('Content-Type', 'application/json');
    }

    // console.log(this.tagDataService, JSON.stringify(myheaders));

    let requestOptions = new RequestOptions({
      headers: myheaders,
      method: _method,
      body: params
    });

    console.log('urlToSend : ' + urlToSend);
    console.log('params : ' + params);
    console.log('method : ' + _method);

    if(_responsetype === "text"){
    return this.http.request(urlToSend, requestOptions)
                      .catch(this.handleError);
    } else {
    return this.http.request(urlToSend, requestOptions)
                    .map(this.extractData)
                    .catch(this.handleError);
    }
  }


  private extractData(res: Response) {
    var defaultServerTarget, responseType, tagDataService;
    console.log('extractData res : ' + res);
    console.log('extractData : ' + tagDataService + res);
    if (responseType === "text"){
      return res;
    }
    else {
      let body:Array<JSON> = res.json();
      return body || { };
    }
  }


  private handleError (error: Response | any) {
    var defaultServerTarget, responseType, tagDataService;
    console.log("Data Service: Can't read response");
    // In a real world app, you might use a remote logging infrastructure
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText} ${err}`;
      console.log('errMsg : ' + errMsg);
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.log(tagDataService, new Error(errMsg));
    return Observable.throw(errMsg);
  }
}
