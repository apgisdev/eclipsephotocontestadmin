import { Component } from "@angular/core"

@Component({
  selector: "loginProvider",
  // templateUrl: "pages/pages/app.html",
  // styleUrls: ["pages/app/app.css"]
})

export class LoginProvider {

  public login = {
    username: "",
    admin: false,
    isVisible: false,
    id: -1,
    subscriptions: [],
    availableEventGroups: []
  }

  public constructor() {

  }

}
