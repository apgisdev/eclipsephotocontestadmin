"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
var http_2 = require("@angular/http");
var Observable_1 = require("rxjs/Observable");
require("rxjs/add/operator/catch");
require("rxjs/add/operator/map");
require("rxjs/add/observable/throw");
var DataService = (function () {
    // private defaultServerTarget = "apnsgis3.apsu.edu/apcon";
    // private defaultServerTarget = "http://localhost:1337";
    function DataService(http) {
        this.http = http;
        this.sandBoxUrl = "http://10.11.207.2:1337"; //http://10.11.207.2:1337
        this.LocalHostUrl = "http://localhost:1337";
        this.tagDataService = "DataService: ";
        this.responseType = "JSON";
        // private defaultServerTarget = "https://apnsgis3.apsu.edu:8080";
        this.defaultServerTarget = "http://apnsgis3.apsu.edu:8080";
    }
    DataService.prototype.requestData = function (_method, _endpoint, _url, _responsetype, _params) {
        console.log('_url : ' + _url);
        //Check if _params was passed and if not, make it empty.
        var params;
        if (_params) {
            params = _params;
        }
        else {
            params = "";
        }
        //Check if _url was passed and if not, make it the sandboxUrl
        var urlToSend;
        urlToSend = _url + _endpoint;
        if (_url || _url !== '') {
            urlToSend = _url + _endpoint;
        }
        else {
            urlToSend = this.defaultServerTarget + _endpoint;
        }
        console.log('urlToSend : ' + urlToSend);
        var myheaders = new http_2.Headers();
        myheaders.set('x-authorization', 'B9CFAE624E2F1F5981738140B1E0259A2EFD15B4073148AD61664073FFBE4357849EEE4C97126D2887539C00C8768516543999F5E0777428');
        if (_responsetype === "text") {
            console.log('endpoint: ' + _endpoint + 'Content-Type text/plain');
            myheaders.append('Content-Type', 'text/plain');
            this.responseType = "text";
        }
        else {
            console.log('endpoint: ' + _endpoint + 'Content-Type application/json');
            myheaders.append('Content-Type', 'application/json');
        }
        console.log(this.tagDataService, JSON.stringify(myheaders));
        var requestOptions = new http_2.RequestOptions({
            headers: myheaders,
            method: _method,
            body: params
        });
        console.log('urlToSend : ' + urlToSend);
        console.log('params : ' + params);
        console.log('method : ' + _method);
        if (_responsetype === "text") {
            return this.http.request(urlToSend, requestOptions)
                .catch(this.handleError);
        }
        else {
            return this.http.request(urlToSend, requestOptions)
                .map(this.extractData)
                .catch(this.handleError);
        }
    };
    DataService.prototype.extractData = function (res) {
        console.log('extractData res : ' + res);
        console.log('extractData : ' + this.tagDataService + res);
        if (this.responseType === "text") {
            return res;
        }
        else {
            var body = res.json();
            return body || {};
        }
    };
    DataService.prototype.handleError = function (error) {
        console.log("Data Service: Can't read response");
        // In a real world app, you might use a remote logging infrastructure
        var errMsg;
        console.log("2 Data Service: Can't read response");
        if (error instanceof http_1.Response) {
            console.log('hello');
            console.log('error : ' + error);
            // console.log(this.tagDataService, error);
            console.log('2 hello');
            var body = error.json() || '';
            console.log('3 hello');
            var err = body.error || JSON.stringify(body);
            console.log('4 hello');
            errMsg = error.status + " - " + error.statusText + " " + err;
            console.log('errMsg : ' + errMsg);
        }
        else {
            errMsg = error.message ? error.message : error.toString();
        }
        console.log('before throwing back an error observable');
        console.log(this.tagDataService, new Error(errMsg));
        console.log('2 before throwing back an error observable');
        return Observable_1.Observable.throw(errMsg);
    };
    return DataService;
}());
DataService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [http_1.Http])
], DataService);
exports.DataService = DataService;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGF0YS5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiZGF0YS5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsc0NBQTJDO0FBQzNDLHNDQUErQztBQUMvQyxzQ0FBd0Q7QUFDeEQsOENBQTZDO0FBQzdDLG1DQUFpQztBQUNqQyxpQ0FBK0I7QUFDL0IscUNBQW1DO0FBR25DLElBQWEsV0FBVztJQVF0QiwyREFBMkQ7SUFDM0QseURBQXlEO0lBRXpELHFCQUFxQixJQUFVO1FBQVYsU0FBSSxHQUFKLElBQUksQ0FBTTtRQVZ2QixlQUFVLEdBQUcseUJBQXlCLENBQUMsQ0FBQSx5QkFBeUI7UUFDaEUsaUJBQVksR0FBRyx1QkFBdUIsQ0FBQztRQUN2QyxtQkFBYyxHQUFHLGVBQWUsQ0FBQztRQUNqQyxpQkFBWSxHQUFHLE1BQU0sQ0FBQztRQUU5QixrRUFBa0U7UUFDMUQsd0JBQW1CLEdBQUcsK0JBQStCLENBQUM7SUFJNUIsQ0FBQztJQUNuQyxpQ0FBVyxHQUFYLFVBQVksT0FBYyxFQUFFLFNBQWdCLEVBQUUsSUFBWSxFQUFFLGFBQXFCLEVBQUUsT0FBZTtRQUVoRyxPQUFPLENBQUMsR0FBRyxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsQ0FBQztRQUM5Qix3REFBd0Q7UUFDeEQsSUFBSSxNQUFNLENBQUM7UUFDWCxFQUFFLENBQUEsQ0FBQyxPQUFPLENBQUMsQ0FBQSxDQUFDO1lBQUMsTUFBTSxHQUFHLE9BQU8sQ0FBQztRQUFBLENBQUM7UUFBQyxJQUFJLENBQUMsQ0FBQztZQUFBLE1BQU0sR0FBRyxFQUFFLENBQUE7UUFBQSxDQUFDO1FBRWxELDZEQUE2RDtRQUM3RCxJQUFJLFNBQVMsQ0FBQztRQUNkLFNBQVMsR0FBRyxJQUFJLEdBQUcsU0FBUyxDQUFDO1FBQzdCLEVBQUUsQ0FBQSxDQUFDLElBQUksSUFBSSxJQUFJLEtBQUssRUFBRSxDQUFDLENBQUEsQ0FBQztZQUFBLFNBQVMsR0FBRyxJQUFJLEdBQUcsU0FBUyxDQUFBO1FBQUEsQ0FBQztRQUFDLElBQUksQ0FBQyxDQUFDO1lBQUEsU0FBUyxHQUFHLElBQUksQ0FBQyxtQkFBbUIsR0FBRyxTQUFTLENBQUM7UUFBQSxDQUFDO1FBQzlHLE9BQU8sQ0FBQyxHQUFHLENBQUMsY0FBYyxHQUFHLFNBQVMsQ0FBQyxDQUFDO1FBRXhDLElBQUksU0FBUyxHQUFHLElBQUksY0FBTyxFQUFFLENBQUM7UUFDOUIsU0FBUyxDQUFDLEdBQUcsQ0FBQyxpQkFBaUIsRUFBRSxrSEFBa0gsQ0FBQyxDQUFDO1FBQ3JKLEVBQUUsQ0FBQSxDQUFDLGFBQWEsS0FBSyxNQUFNLENBQUMsQ0FBQSxDQUFDO1lBQzNCLE9BQU8sQ0FBQyxHQUFHLENBQUMsWUFBWSxHQUFDLFNBQVMsR0FBQyx5QkFBeUIsQ0FBQyxDQUFDO1lBQzlELFNBQVMsQ0FBQyxNQUFNLENBQUMsY0FBYyxFQUFFLFlBQVksQ0FBQyxDQUFDO1lBQy9DLElBQUksQ0FBQyxZQUFZLEdBQUcsTUFBTSxDQUFBO1FBQzVCLENBQUM7UUFBQyxJQUFJLENBQUMsQ0FBQztZQUNOLE9BQU8sQ0FBQyxHQUFHLENBQUMsWUFBWSxHQUFDLFNBQVMsR0FBQywrQkFBK0IsQ0FBQyxDQUFDO1lBQ3BFLFNBQVMsQ0FBQyxNQUFNLENBQUMsY0FBYyxFQUFFLGtCQUFrQixDQUFDLENBQUM7UUFDdkQsQ0FBQztRQUVELE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLGNBQWMsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUM7UUFFNUQsSUFBSSxjQUFjLEdBQUcsSUFBSSxxQkFBYyxDQUFDO1lBQ3RDLE9BQU8sRUFBRSxTQUFTO1lBQ2xCLE1BQU0sRUFBRSxPQUFPO1lBQ2YsSUFBSSxFQUFFLE1BQU07U0FDYixDQUFDLENBQUM7UUFFSCxPQUFPLENBQUMsR0FBRyxDQUFDLGNBQWMsR0FBRyxTQUFTLENBQUMsQ0FBQztRQUN4QyxPQUFPLENBQUMsR0FBRyxDQUFDLFdBQVcsR0FBRyxNQUFNLENBQUMsQ0FBQztRQUNsQyxPQUFPLENBQUMsR0FBRyxDQUFDLFdBQVcsR0FBRyxPQUFPLENBQUMsQ0FBQztRQUVuQyxFQUFFLENBQUEsQ0FBQyxhQUFhLEtBQUssTUFBTSxDQUFDLENBQUEsQ0FBQztZQUM3QixNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsU0FBUyxFQUFFLGNBQWMsQ0FBQztpQkFDaEMsS0FBSyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQztRQUMzQyxDQUFDO1FBQUMsSUFBSSxDQUFDLENBQUM7WUFDUixNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsU0FBUyxFQUFFLGNBQWMsQ0FBQztpQkFDbEMsR0FBRyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUM7aUJBQ3JCLEtBQUssQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7UUFDekMsQ0FBQztJQUNILENBQUM7SUFHTyxpQ0FBVyxHQUFuQixVQUFvQixHQUFhO1FBQy9CLE9BQU8sQ0FBQyxHQUFHLENBQUMsb0JBQW9CLEdBQUcsR0FBRyxDQUFDLENBQUM7UUFDeEMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxnQkFBZ0IsR0FBRyxJQUFJLENBQUMsY0FBYyxHQUFHLEdBQUcsQ0FBQyxDQUFDO1FBQzFELEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxZQUFZLEtBQUssTUFBTSxDQUFDLENBQUEsQ0FBQztZQUNoQyxNQUFNLENBQUMsR0FBRyxDQUFDO1FBQ2IsQ0FBQztRQUNELElBQUksQ0FBQyxDQUFDO1lBQ0osSUFBSSxJQUFJLEdBQWUsR0FBRyxDQUFDLElBQUksRUFBRSxDQUFDO1lBQ2xDLE1BQU0sQ0FBQyxJQUFJLElBQUksRUFBRyxDQUFDO1FBQ3JCLENBQUM7SUFDSCxDQUFDO0lBR08saUNBQVcsR0FBbkIsVUFBcUIsS0FBcUI7UUFDeEMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxtQ0FBbUMsQ0FBQyxDQUFDO1FBQ2pELHFFQUFxRTtRQUNyRSxJQUFJLE1BQWMsQ0FBQztRQUNuQixPQUFPLENBQUMsR0FBRyxDQUFDLHFDQUFxQyxDQUFDLENBQUM7UUFDbkQsRUFBRSxDQUFDLENBQUMsS0FBSyxZQUFZLGVBQVEsQ0FBQyxDQUFDLENBQUM7WUFDOUIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsQ0FBQztZQUNyQixPQUFPLENBQUMsR0FBRyxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUMsQ0FBQztZQUNoQywyQ0FBMkM7WUFDM0MsT0FBTyxDQUFDLEdBQUcsQ0FBQyxTQUFTLENBQUMsQ0FBQztZQUV2QixJQUFNLElBQUksR0FBRyxLQUFLLENBQUMsSUFBSSxFQUFFLElBQUksRUFBRSxDQUFDO1lBQ2hDLE9BQU8sQ0FBQyxHQUFHLENBQUMsU0FBUyxDQUFDLENBQUM7WUFDdkIsSUFBTSxHQUFHLEdBQUcsSUFBSSxDQUFDLEtBQUssSUFBSSxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQy9DLE9BQU8sQ0FBQyxHQUFHLENBQUMsU0FBUyxDQUFDLENBQUM7WUFDdkIsTUFBTSxHQUFNLEtBQUssQ0FBQyxNQUFNLFdBQU0sS0FBSyxDQUFDLFVBQVUsU0FBSSxHQUFLLENBQUM7WUFDeEQsT0FBTyxDQUFDLEdBQUcsQ0FBQyxXQUFXLEdBQUcsTUFBTSxDQUFDLENBQUM7UUFFcEMsQ0FBQztRQUFDLElBQUksQ0FBQyxDQUFDO1lBQ04sTUFBTSxHQUFHLEtBQUssQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUMsUUFBUSxFQUFFLENBQUM7UUFDNUQsQ0FBQztRQUNELE9BQU8sQ0FBQyxHQUFHLENBQUMsMENBQTBDLENBQUMsQ0FBQztRQUN4RCxPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxjQUFjLEVBQUUsSUFBSSxLQUFLLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztRQUNwRCxPQUFPLENBQUMsR0FBRyxDQUFDLDRDQUE0QyxDQUFDLENBQUM7UUFDMUQsTUFBTSxDQUFDLHVCQUFVLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBQ2xDLENBQUM7SUFFSCxrQkFBQztBQUFELENBQUMsQUFuR0QsSUFtR0M7QUFuR1ksV0FBVztJQUR2QixpQkFBVSxFQUFFO3FDQVlnQixXQUFJO0dBWHBCLFdBQVcsQ0FtR3ZCO0FBbkdZLGtDQUFXIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgSHR0cCwgUmVzcG9uc2UgfSBmcm9tICdAYW5ndWxhci9odHRwJztcbmltcG9ydCB7IEhlYWRlcnMsIFJlcXVlc3RPcHRpb25zIH0gZnJvbSAnQGFuZ3VsYXIvaHR0cCc7XG5pbXBvcnQgeyBPYnNlcnZhYmxlIH0gZnJvbSAncnhqcy9PYnNlcnZhYmxlJztcbmltcG9ydCAncnhqcy9hZGQvb3BlcmF0b3IvY2F0Y2gnO1xuaW1wb3J0ICdyeGpzL2FkZC9vcGVyYXRvci9tYXAnO1xuaW1wb3J0ICdyeGpzL2FkZC9vYnNlcnZhYmxlL3Rocm93JztcblxuQEluamVjdGFibGUoKVxuZXhwb3J0IGNsYXNzIERhdGFTZXJ2aWNlIHtcbiAgcHJpdmF0ZSBzYW5kQm94VXJsID0gXCJodHRwOi8vMTAuMTEuMjA3LjI6MTMzN1wiOy8vaHR0cDovLzEwLjExLjIwNy4yOjEzMzdcbiAgcHJpdmF0ZSBMb2NhbEhvc3RVcmwgPSBcImh0dHA6Ly9sb2NhbGhvc3Q6MTMzN1wiO1xuICBwcml2YXRlIHRhZ0RhdGFTZXJ2aWNlID0gXCJEYXRhU2VydmljZTogXCI7XG4gIHByaXZhdGUgcmVzcG9uc2VUeXBlID0gXCJKU09OXCI7XG5cbiAgLy8gcHJpdmF0ZSBkZWZhdWx0U2VydmVyVGFyZ2V0ID0gXCJodHRwczovL2FwbnNnaXMzLmFwc3UuZWR1OjgwODBcIjtcbiAgcHJpdmF0ZSBkZWZhdWx0U2VydmVyVGFyZ2V0ID0gXCJodHRwOi8vYXBuc2dpczMuYXBzdS5lZHU6ODA4MFwiO1xuICAvLyBwcml2YXRlIGRlZmF1bHRTZXJ2ZXJUYXJnZXQgPSBcImFwbnNnaXMzLmFwc3UuZWR1L2FwY29uXCI7XG4gIC8vIHByaXZhdGUgZGVmYXVsdFNlcnZlclRhcmdldCA9IFwiaHR0cDovL2xvY2FsaG9zdDoxMzM3XCI7XG5cbiAgY29uc3RydWN0b3IgKHByaXZhdGUgaHR0cDogSHR0cCkge31cbiAgcmVxdWVzdERhdGEoX21ldGhvZDpzdHJpbmcsIF9lbmRwb2ludDpzdHJpbmcsIF91cmw/OnN0cmluZywgX3Jlc3BvbnNldHlwZT86c3RyaW5nLCBfcGFyYW1zPzpzdHJpbmcpOiBPYnNlcnZhYmxlPGFueT4ge1xuXG4gICAgY29uc29sZS5sb2coJ191cmwgOiAnICsgX3VybCk7XG4gICAgLy9DaGVjayBpZiBfcGFyYW1zIHdhcyBwYXNzZWQgYW5kIGlmIG5vdCwgbWFrZSBpdCBlbXB0eS5cbiAgICBsZXQgcGFyYW1zO1xuICAgIGlmKF9wYXJhbXMpeyBwYXJhbXMgPSBfcGFyYW1zO30gZWxzZSB7cGFyYW1zID0gXCJcIn1cblxuICAgIC8vQ2hlY2sgaWYgX3VybCB3YXMgcGFzc2VkIGFuZCBpZiBub3QsIG1ha2UgaXQgdGhlIHNhbmRib3hVcmxcbiAgICBsZXQgdXJsVG9TZW5kO1xuICAgIHVybFRvU2VuZCA9IF91cmwgKyBfZW5kcG9pbnQ7XG4gICAgaWYoX3VybCB8fCBfdXJsICE9PSAnJyl7dXJsVG9TZW5kID0gX3VybCArIF9lbmRwb2ludH0gZWxzZSB7dXJsVG9TZW5kID0gdGhpcy5kZWZhdWx0U2VydmVyVGFyZ2V0ICsgX2VuZHBvaW50O31cbiAgICBjb25zb2xlLmxvZygndXJsVG9TZW5kIDogJyArIHVybFRvU2VuZCk7XG5cbiAgICBsZXQgbXloZWFkZXJzID0gbmV3IEhlYWRlcnMoKTtcbiAgICBteWhlYWRlcnMuc2V0KCd4LWF1dGhvcml6YXRpb24nLCAnQjlDRkFFNjI0RTJGMUY1OTgxNzM4MTQwQjFFMDI1OUEyRUZEMTVCNDA3MzE0OEFENjE2NjQwNzNGRkJFNDM1Nzg0OUVFRTRDOTcxMjZEMjg4NzUzOUMwMEM4NzY4NTE2NTQzOTk5RjVFMDc3NzQyOCcpO1xuICAgIGlmKF9yZXNwb25zZXR5cGUgPT09IFwidGV4dFwiKXtcbiAgICAgIGNvbnNvbGUubG9nKCdlbmRwb2ludDogJytfZW5kcG9pbnQrJ0NvbnRlbnQtVHlwZSB0ZXh0L3BsYWluJyk7XG4gICAgICBteWhlYWRlcnMuYXBwZW5kKCdDb250ZW50LVR5cGUnLCAndGV4dC9wbGFpbicpO1xuICAgICAgdGhpcy5yZXNwb25zZVR5cGUgPSBcInRleHRcIlxuICAgIH0gZWxzZSB7XG4gICAgICBjb25zb2xlLmxvZygnZW5kcG9pbnQ6ICcrX2VuZHBvaW50KydDb250ZW50LVR5cGUgYXBwbGljYXRpb24vanNvbicpO1xuICAgICAgbXloZWFkZXJzLmFwcGVuZCgnQ29udGVudC1UeXBlJywgJ2FwcGxpY2F0aW9uL2pzb24nKTtcbiAgICB9XG5cbiAgICBjb25zb2xlLmxvZyh0aGlzLnRhZ0RhdGFTZXJ2aWNlLCBKU09OLnN0cmluZ2lmeShteWhlYWRlcnMpKTtcblxuICAgIGxldCByZXF1ZXN0T3B0aW9ucyA9IG5ldyBSZXF1ZXN0T3B0aW9ucyh7XG4gICAgICBoZWFkZXJzOiBteWhlYWRlcnMsXG4gICAgICBtZXRob2Q6IF9tZXRob2QsXG4gICAgICBib2R5OiBwYXJhbXNcbiAgICB9KTtcblxuICAgIGNvbnNvbGUubG9nKCd1cmxUb1NlbmQgOiAnICsgdXJsVG9TZW5kKTtcbiAgICBjb25zb2xlLmxvZygncGFyYW1zIDogJyArIHBhcmFtcyk7XG4gICAgY29uc29sZS5sb2coJ21ldGhvZCA6ICcgKyBfbWV0aG9kKTtcblxuICAgIGlmKF9yZXNwb25zZXR5cGUgPT09IFwidGV4dFwiKXtcbiAgICByZXR1cm4gdGhpcy5odHRwLnJlcXVlc3QodXJsVG9TZW5kLCByZXF1ZXN0T3B0aW9ucylcbiAgICAgICAgICAgICAgICAgICAgICAuY2F0Y2godGhpcy5oYW5kbGVFcnJvcik7XG4gICAgfSBlbHNlIHtcbiAgICByZXR1cm4gdGhpcy5odHRwLnJlcXVlc3QodXJsVG9TZW5kLCByZXF1ZXN0T3B0aW9ucylcbiAgICAgICAgICAgICAgICAgICAgLm1hcCh0aGlzLmV4dHJhY3REYXRhKVxuICAgICAgICAgICAgICAgICAgICAuY2F0Y2godGhpcy5oYW5kbGVFcnJvcik7XG4gICAgfVxuICB9XG5cblxuICBwcml2YXRlIGV4dHJhY3REYXRhKHJlczogUmVzcG9uc2UpIHtcbiAgICBjb25zb2xlLmxvZygnZXh0cmFjdERhdGEgcmVzIDogJyArIHJlcyk7XG4gICAgY29uc29sZS5sb2coJ2V4dHJhY3REYXRhIDogJyArIHRoaXMudGFnRGF0YVNlcnZpY2UgKyByZXMpO1xuICAgIGlmICh0aGlzLnJlc3BvbnNlVHlwZSA9PT0gXCJ0ZXh0XCIpe1xuICAgICAgcmV0dXJuIHJlcztcbiAgICB9XG4gICAgZWxzZSB7XG4gICAgICBsZXQgYm9keTpBcnJheTxKU09OPiA9IHJlcy5qc29uKCk7XG4gICAgICByZXR1cm4gYm9keSB8fCB7IH07XG4gICAgfVxuICB9XG5cblxuICBwcml2YXRlIGhhbmRsZUVycm9yIChlcnJvcjogUmVzcG9uc2UgfCBhbnkpIHtcbiAgICBjb25zb2xlLmxvZyhcIkRhdGEgU2VydmljZTogQ2FuJ3QgcmVhZCByZXNwb25zZVwiKTtcbiAgICAvLyBJbiBhIHJlYWwgd29ybGQgYXBwLCB5b3UgbWlnaHQgdXNlIGEgcmVtb3RlIGxvZ2dpbmcgaW5mcmFzdHJ1Y3R1cmVcbiAgICBsZXQgZXJyTXNnOiBzdHJpbmc7XG4gICAgY29uc29sZS5sb2coXCIyIERhdGEgU2VydmljZTogQ2FuJ3QgcmVhZCByZXNwb25zZVwiKTtcbiAgICBpZiAoZXJyb3IgaW5zdGFuY2VvZiBSZXNwb25zZSkge1xuICAgICAgY29uc29sZS5sb2coJ2hlbGxvJyk7XG4gICAgICBjb25zb2xlLmxvZygnZXJyb3IgOiAnICsgZXJyb3IpO1xuICAgICAgLy8gY29uc29sZS5sb2codGhpcy50YWdEYXRhU2VydmljZSwgZXJyb3IpO1xuICAgICAgY29uc29sZS5sb2coJzIgaGVsbG8nKTtcblxuICAgICAgY29uc3QgYm9keSA9IGVycm9yLmpzb24oKSB8fCAnJztcbiAgICAgIGNvbnNvbGUubG9nKCczIGhlbGxvJyk7XG4gICAgICBjb25zdCBlcnIgPSBib2R5LmVycm9yIHx8IEpTT04uc3RyaW5naWZ5KGJvZHkpO1xuICAgICAgY29uc29sZS5sb2coJzQgaGVsbG8nKTtcbiAgICAgIGVyck1zZyA9IGAke2Vycm9yLnN0YXR1c30gLSAke2Vycm9yLnN0YXR1c1RleHR9ICR7ZXJyfWA7XG4gICAgICBjb25zb2xlLmxvZygnZXJyTXNnIDogJyArIGVyck1zZyk7XG5cbiAgICB9IGVsc2Uge1xuICAgICAgZXJyTXNnID0gZXJyb3IubWVzc2FnZSA/IGVycm9yLm1lc3NhZ2UgOiBlcnJvci50b1N0cmluZygpO1xuICAgIH1cbiAgICBjb25zb2xlLmxvZygnYmVmb3JlIHRocm93aW5nIGJhY2sgYW4gZXJyb3Igb2JzZXJ2YWJsZScpO1xuICAgIGNvbnNvbGUubG9nKHRoaXMudGFnRGF0YVNlcnZpY2UsIG5ldyBFcnJvcihlcnJNc2cpKTtcbiAgICBjb25zb2xlLmxvZygnMiBiZWZvcmUgdGhyb3dpbmcgYmFjayBhbiBlcnJvciBvYnNlcnZhYmxlJyk7XG4gICAgcmV0dXJuIE9ic2VydmFibGUudGhyb3coZXJyTXNnKTtcbiAgfVxuXG59XG4iXX0=