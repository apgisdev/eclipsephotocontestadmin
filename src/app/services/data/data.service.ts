import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';

@Injectable()
export class DataService {
  private sandBoxUrl = "http://10.11.207.2:1337";//http://10.11.207.2:1337
  private LocalHostUrl = "http://localhost:1337";
  private tagDataService = "DataService: ";
  private responseType = "JSON";

  // private defaultServerTarget = "https://apnsgis3.apsu.edu:8080";
  private defaultServerTarget = "http://apnsgis3.apsu.edu:8080";
  // private defaultServerTarget = "apnsgis3.apsu.edu/apcon";
  // private defaultServerTarget = "http://localhost:1337";

  constructor (private http: Http) {}
  requestData(_method:string, _endpoint:string, _url?:string, _responsetype?:string, _params?:string): Observable<any> {

    console.log('_url : ' + _url);
    //Check if _params was passed and if not, make it empty.
    let params;
    if(_params){ params = _params;} else {params = ""}

    //Check if _url was passed and if not, make it the sandboxUrl
    let urlToSend;
    urlToSend = _url + _endpoint;
    if(_url || _url !== ''){urlToSend = _url + _endpoint} else {urlToSend = this.defaultServerTarget + _endpoint;}
    console.log('urlToSend : ' + urlToSend);

    let myheaders = new Headers();
    myheaders.set('x-authorization', 'B9CFAE624E2F1F5981738140B1E0259A2EFD15B4073148AD61664073FFBE4357849EEE4C97126D2887539C00C8768516543999F5E0777428');
    if(_responsetype === "text"){
      console.log('endpoint: '+_endpoint+'Content-Type text/plain');
      myheaders.append('Content-Type', 'text/plain');
      this.responseType = "text"
    } else {
      console.log('endpoint: '+_endpoint+'Content-Type application/json');
      myheaders.append('Content-Type', 'application/json');
    }

    console.log(this.tagDataService, JSON.stringify(myheaders));

    let requestOptions = new RequestOptions({
      headers: myheaders,
      method: _method,
      body: params
    });

    console.log('urlToSend : ' + urlToSend);
    console.log('params : ' + params);
    console.log('method : ' + _method);

    if(_responsetype === "text"){
    return this.http.request(urlToSend, requestOptions)
                      .catch(this.handleError);
    } else {
    return this.http.request(urlToSend, requestOptions)
                    .map(this.extractData)
                    .catch(this.handleError);
    }
  }


  private extractData(res: Response) {
    console.log('extractData res : ' + res);
    console.log('extractData : ' + this.tagDataService + res);
    if (this.responseType === "text"){
      return res;
    }
    else {
      let body:Array<JSON> = res.json();
      return body || { };
    }
  }


  private handleError (error: Response | any) {
    console.log("Data Service: Can't read response");
    // In a real world app, you might use a remote logging infrastructure
    let errMsg: string;
    console.log("2 Data Service: Can't read response");
    if (error instanceof Response) {
      console.log('hello');
      console.log('error : ' + error);
      // console.log(this.tagDataService, error);
      console.log('2 hello');

      const body = error.json() || '';
      console.log('3 hello');
      const err = body.error || JSON.stringify(body);
      console.log('4 hello');
      errMsg = `${error.status} - ${error.statusText} ${err}`;
      console.log('errMsg : ' + errMsg);

    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.log('before throwing back an error observable');
    console.log(this.tagDataService, new Error(errMsg));
    console.log('2 before throwing back an error observable');
    return Observable.throw(errMsg);
  }

}
